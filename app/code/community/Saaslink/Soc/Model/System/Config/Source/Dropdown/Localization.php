<?php

class Saaslink_Soc_Model_System_Config_Source_Dropdown_Localization
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'af_ZA', 'label'=>Mage::helper('soc')->__('Afrikaans')),
			array('value'=>'sq_AL', 'label'=>Mage::helper('soc')->__('Albanian')),
			array('value'=>'ar_AR', 'label'=>Mage::helper('soc')->__('Arabic')),
			array('value'=>'hy_AM', 'label'=>Mage::helper('soc')->__('Armenian')),
			array('value'=>'ay_BO', 'label'=>Mage::helper('soc')->__('Aymara')),
			array('value'=>'az_AZ', 'label'=>Mage::helper('soc')->__('Azeri')),
			array('value'=>'be_BY', 'label'=>Mage::helper('soc')->__('Belarusian')),
			array('value'=>'bn_IN', 'label'=>Mage::helper('soc')->__('Bengali')),
			array('value'=>'bs_BA', 'label'=>Mage::helper('soc')->__('Bosnian')),
			array('value'=>'bg_BG', 'label'=>Mage::helper('soc')->__('Bulgarian')),
			array('value'=>'hr_HR', 'label'=>Mage::helper('soc')->__('Croatian')),
			array('value'=>'ca_ES', 'label'=>Mage::helper('soc')->__('Catalan')),
			array('value'=>'cs_CZ', 'label'=>Mage::helper('soc')->__('Czech')),
			array('value'=>'da_DK', 'label'=>Mage::helper('soc')->__('Danish')),
			array('value'=>'nl_NL', 'label'=>Mage::helper('soc')->__('Dutch')),
			array('value'=>'nl_BE', 'label'=>Mage::helper('soc')->__('Dutch(Belgi?)')),
			array('value'=>'en_US', 'label'=>Mage::helper('soc')->__('English(US)')),
			array('value'=>'en_GB', 'label'=>Mage::helper('soc')->__('English(UK)')),
			array('value'=>'en_PI', 'label'=>Mage::helper('soc')->__('English(Pirate)')),
			array('value'=>'en_UD', 'label'=>Mage::helper('soc')->__('English(UpsideDown)')),
			array('value'=>'eo_EO', 'label'=>Mage::helper('soc')->__('Esperanto')),
			array('value'=>'et_EE', 'label'=>Mage::helper('soc')->__('Estonian')),
			array('value'=>'fo_FO', 'label'=>Mage::helper('soc')->__('Faroese')),
			array('value'=>'fr_CA', 'label'=>Mage::helper('soc')->__('French(Canada)')),
			array('value'=>'fb_FI', 'label'=>Mage::helper('soc')->__('Finnish(test)')),
			array('value'=>'fi_FI', 'label'=>Mage::helper('soc')->__('Finnish')),
			array('value'=>'fr_FR', 'label'=>Mage::helper('soc')->__('French(France)')),
			array('value'=>'gl_ES', 'label'=>Mage::helper('soc')->__('Galician')),
			array('value'=>'ka_GE', 'label'=>Mage::helper('soc')->__('Georgian')),
			array('value'=>'el_GR', 'label'=>Mage::helper('soc')->__('Greek')),
			array('value'=>'gu_IN', 'label'=>Mage::helper('soc')->__('Gujarati')),
			array('value'=>'he_IL', 'label'=>Mage::helper('soc')->__('Hebrew')),
			array('value'=>'hu_HU', 'label'=>Mage::helper('soc')->__('Hungarian')),
			array('value'=>'hi_IN', 'label'=>Mage::helper('soc')->__('Hindi')),
			array('value'=>'is_IS', 'label'=>Mage::helper('soc')->__('Icelandic')),
			array('value'=>'id_ID', 'label'=>Mage::helper('soc')->__('Indonesian')),
			array('value'=>'ga_IE', 'label'=>Mage::helper('soc')->__('Irish')),
			array('value'=>'jv_ID', 'label'=>Mage::helper('soc')->__('Javanese')),
			array('value'=>'kn_IN', 'label'=>Mage::helper('soc')->__('Kannada')),
			array('value'=>'kk_KZ', 'label'=>Mage::helper('soc')->__('Kazakh')),
			array('value'=>'la_VA', 'label'=>Mage::helper('soc')->__('Latin')),
			array('value'=>'lv_LV', 'label'=>Mage::helper('soc')->__('Latvian')),
			array('value'=>'li_NL', 'label'=>Mage::helper('soc')->__('Limburgish')),
			array('value'=>'lt_LT', 'label'=>Mage::helper('soc')->__('Lithuanian')),
			array('value'=>'mk_MK', 'label'=>Mage::helper('soc')->__('Macedonian')),
			array('value'=>'mg_MG', 'label'=>Mage::helper('soc')->__('Malagasy')),
			array('value'=>'ms_MY', 'label'=>Mage::helper('soc')->__('Malay')),
			array('value'=>'mt_MT', 'label'=>Mage::helper('soc')->__('Maltese')),
			array('value'=>'mr_IN', 'label'=>Mage::helper('soc')->__('Marathi')),
			array('value'=>'mn_MN', 'label'=>Mage::helper('soc')->__('Mongolian')),
			array('value'=>'ne_NP', 'label'=>Mage::helper('soc')->__('Nepali')),
			array('value'=>'nb_NO', 'label'=>Mage::helper('soc')->__('Norwegian(bokmal)')),
			array('value'=>'nn_NO', 'label'=>Mage::helper('soc')->__('Norwegian(nynorsk)')),
			array('value'=>'ps_AF', 'label'=>Mage::helper('soc')->__('Pashto')),
			array('value'=>'pl_PL', 'label'=>Mage::helper('soc')->__('Polish')),
			array('value'=>'pt_BR', 'label'=>Mage::helper('soc')->__('Portuguese(Brazil)')),
			array('value'=>'pt_PT', 'label'=>Mage::helper('soc')->__('Portuguese(Portugal)')),
			array('value'=>'pa_IN', 'label'=>Mage::helper('soc')->__('Punjabi')),
			array('value'=>'rm_CH', 'label'=>Mage::helper('soc')->__('Romansh')),
			array('value'=>'sa_IN', 'label'=>Mage::helper('soc')->__('Sanskrit')),
			array('value'=>'sr_RS', 'label'=>Mage::helper('soc')->__('Serbian')),
			array('value'=>'so_SO', 'label'=>Mage::helper('soc')->__('Somali')),
			array('value'=>'es_LA', 'label'=>Mage::helper('soc')->__('Spanish')),
			array('value'=>'es_CL', 'label'=>Mage::helper('soc')->__('Spanish(Chile)')),
			array('value'=>'es_CO', 'label'=>Mage::helper('soc')->__('Spanish(Colombia)')),
			array('value'=>'es_ES', 'label'=>Mage::helper('soc')->__('Spanish(Spain)')),
			array('value'=>'es_MX', 'label'=>Mage::helper('soc')->__('Spanish(Mexico)')),
			array('value'=>'es_VE', 'label'=>Mage::helper('soc')->__('Spanish(Venezuela)')),
			array('value'=>'sw_KE', 'label'=>Mage::helper('soc')->__('Swahili')),
			array('value'=>'tl_PH', 'label'=>Mage::helper('soc')->__('Filipino')),
			array('value'=>'tg_TJ', 'label'=>Mage::helper('soc')->__('Tajik')),
			array('value'=>'ta_IN', 'label'=>Mage::helper('soc')->__('Tamil')),
			array('value'=>'tt_RU', 'label'=>Mage::helper('soc')->__('Tatar')),
			array('value'=>'te_IN', 'label'=>Mage::helper('soc')->__('Telugu')),
			array('value'=>'ml_IN', 'label'=>Mage::helper('soc')->__('Malayalam')),
			array('value'=>'uk_UA', 'label'=>Mage::helper('soc')->__('Ukrainian')),
			array('value'=>'uz_UZ', 'label'=>Mage::helper('soc')->__('Uzbek')),
			array('value'=>'vi_VN', 'label'=>Mage::helper('soc')->__('Vietnamese')),
			array('value'=>'xh_ZA', 'label'=>Mage::helper('soc')->__('Xhosa')),
			array('value'=>'yi_DE', 'label'=>Mage::helper('soc')->__('Yiddish')),
			array('value'=>'zu_ZA', 'label'=>Mage::helper('soc')->__('Zulu')),
		
			array('value'=>'cy_GB', 'label'=>Mage::helper('soc')->__('Welsh')),
			array('value'=>'de_DE', 'label'=>Mage::helper('soc')->__('German')),
			array('value'=>'eu_ES', 'label'=>Mage::helper('soc')->__('Basque')),
			array('value'=>'ck_US', 'label'=>Mage::helper('soc')->__('Cherokee')),
			array('value'=>'it_IT', 'label'=>Mage::helper('soc')->__('Italian')),
			array('value'=>'ja_JP', 'label'=>Mage::helper('soc')->__('Japanese')),
			array('value'=>'ko_KR', 'label'=>Mage::helper('soc')->__('Korean')),
			array('value'=>'ro_RO', 'label'=>Mage::helper('soc')->__('Romanian')),
			array('value'=>'ru_RU', 'label'=>Mage::helper('soc')->__('Russian')),
			array('value'=>'sk_SK', 'label'=>Mage::helper('soc')->__('Slovak')),
			array('value'=>'sl_SI', 'label'=>Mage::helper('soc')->__('Slovenian')),
			array('value'=>'sv_SE', 'label'=>Mage::helper('soc')->__('Swedish')),
			array('value'=>'th_TH', 'label'=>Mage::helper('soc')->__('Thai')),
			array('value'=>'tr_TR', 'label'=>Mage::helper('soc')->__('Turkish')),
			array('value'=>'ku_TR', 'label'=>Mage::helper('soc')->__('Kurdish')),
			array('value'=>'zh_CN', 'label'=>Mage::helper('soc')->__('SimplifiedChinese(China)')),
			array('value'=>'zh_HK', 'label'=>Mage::helper('soc')->__('TraditionalChinese(HongKong)')),
			array('value'=>'zh_TW', 'label'=>Mage::helper('soc')->__('TraditionalChinese(Taiwan)')),
			array('value'=>'fb_LT', 'label'=>Mage::helper('soc')->__('LeetSpeak')),
						
			array('value'=>'km_KH', 'label'=>Mage::helper('soc')->__('Khmer')),
			array('value'=>'ur_PK', 'label'=>Mage::helper('soc')->__('Urdu')),
			array('value'=>'fa_IR', 'label'=>Mage::helper('soc')->__('Persian')),
			array('value'=>'sy_SY', 'label'=>Mage::helper('soc')->__('Syriac')),
			array('value'=>'gn_PY', 'label'=>Mage::helper('soc')->__('Guaran?')),
			array('value'=>'qu_PE', 'label'=>Mage::helper('soc')->__('Quechua')),
			array('value'=>'se_NO', 'label'=>Mage::helper('soc')->__('NorthernS?mi')),
			array('value'=>'tl_ST', 'label'=>Mage::helper('soc')->__('Klingon')),

        );
    }
}
