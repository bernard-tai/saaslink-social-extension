<?php

class Saaslink_Soc_Helper_Data extends Mage_Core_Helper_Abstract
{
	const SOC_FACEBOOK_STOCK_OUT = 'oos';
	const SOC_FACEBOOK_STOCK_IN = 'instock';
	const SOC_WANELO_STOCK_OUT = 'OutOfStock';
	const SOC_WANELO_STOCK_IN = 'InStock';
	const SOC_TWITTER_STOCK_OUT = 'Out of Stock';
	const SOC_TWITTER_STOCK_IN = 'In Stock';
	
}